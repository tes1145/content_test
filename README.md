# content_test

###PAGE list cases:
 - http://0.0.0.0:8000/api/page/
 - http://0.0.0.0:8000/api/page?limit=5
 - http://0.0.0.0:8000/api/page?limit=1&page=2
###PAGE retrieve cases:
 - http://0.0.0.0:8000/api/page/get/1
 - http://0.0.0.0:8000/api/page/get/1?order=desc
###Requirements:
 - docker-compose 2.2.2

###Startup
 On first startup flag --build is required
 - docker-compose up --build autotests
 - docker-compose up runserver
