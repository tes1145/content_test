from pathlib import Path

from django.contrib import admin

import just_media.models as models

current_app = Path(__file__).resolve().parent.name


def content_admin_factory(mdl):
    class_name = f'{mdl._meta.object_name}Admin'
    serializer_class = type(class_name,
                            (admin.ModelAdmin,),
                            {
                                'list_display': mdl.fields()
                            })
    return admin.register(mdl)(serializer_class)


def inline_factory(mdl):
    class_name = f'{mdl._meta.object_name}Inline'
    serializer_class = type(class_name,
                            (admin.TabularInline, ),
                            {"model": mdl})
    return serializer_class


content_admin = [
    content_admin_factory(getattr(models, model_name))
    for model_name in models.content_models
]


@admin.register(models.PageContent)
class PageContentAdmin(admin.ModelAdmin):
    search_fields = ('title__icontains',)
    list_display = models.PageContent.fields()


@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ("id", "title")
    search_fields = ('title__icontains', )
    inlines = [inline_factory(models.PageContent)]
