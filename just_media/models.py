import re

from django.db import models
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation


class ModelFieldsMixin:
    @classmethod
    def fields(cls):
        return [field.attname
                for field in cls._meta.fields
                if re.match(r'(?!\w+_id)', field.attname)]


class Page(models.Model, ModelFieldsMixin):
    title = models.CharField(max_length=512)

    def __str__(self):
        return f'{self.title} ({self.pk})'


class ContentBase(models.Model, ModelFieldsMixin):
    class Meta:
        abstract = True
    page_content = GenericRelation('PageContent')


class Video(ContentBase):
    video_url = models.URLField(max_length=1024)
    sub_url = models.URLField(max_length=1024, null=True, blank=True)


class Audio(ContentBase):
    audio_url = models.URLField(max_length=1024)
    bitrate = models.PositiveIntegerField(null=True)


class Text(ContentBase):
    text = models.TextField()


content_models = [model.__name__ for model in ContentBase.__subclasses__()]

content_models_limit = Q(
    app_label='just_media',
    model__in=[model_name.lower()
               for model_name in content_models]
)


class PageContent(models.Model, ModelFieldsMixin):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    title = models.CharField(max_length=512)
    counter = models.PositiveIntegerField(default=0, editable=False)

    content_type = models.ForeignKey(ContentType,
                                     limit_choices_to=content_models_limit,
                                     on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return f'{self.title} ({self.pk})'




