from functools import wraps

from django.urls import reverse
from rest_framework import serializers

from just_media import models
from just_media.tasks import update_content_counter


def counter_decorator(fn):
    @wraps(fn)
    def decorator(self, instance):
        update_content_counter.apply_async(
            (instance.pk, ))
        return fn(self, instance)
    return decorator


def serializer_factory(mdl):
    class_name = f'{mdl._meta.object_name}Serializer'

    class Meta:
        model = mdl
        fields = mdl.fields()

    serializer_class = type(class_name,
                            (serializers.ModelSerializer, ),
                            {"Meta": Meta})
    return serializer_class


content_serializers = {
    model_name: serializer_factory(getattr(models, model_name))
    for model_name in models.content_models
}


class PageContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PageContent
        fields = ['data'] + models.PageContent.fields()

    data = serializers.SerializerMethodField()

    def get_data(self, obj):
        serializer = content_serializers[
            obj.content_object._meta.object_name
        ](obj.content_object)
        return serializer.data

    @counter_decorator
    def to_representation(self, instance):
        return super().to_representation(instance)


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Page
        fields = ['title', 'content']
    order = 'asc'
    content = serializers.SerializerMethodField()

    def get_content(self, obj):
        result = []
        content_set = models.PageContent.objects.filter(
            page_id=obj.id
        ).order_by(['id', '-id'][self.order == 'desc'])
        for item in content_set:
            serializer = PageContentSerializer(item)
            result.append(serializer.data)
        return result


class SimplePageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Page
        fields = ['title', 'url']
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return reverse('page-detail', args=(obj.pk, ))

