from celery import shared_task
from django.db.models import F
from django.db import transaction

from just_media import models


@shared_task(bind=True)
def update_content_counter(self, content_id):
    with transaction.atomic():
        models.PageContent.objects.filter(
            pk=content_id
        ).update(counter=F('counter') + 1)
