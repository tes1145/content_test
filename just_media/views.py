from urllib.parse import urlparse

from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from rest_framework import viewsets, routers, pagination

from just_media import models, serializers


class ReadOnlyRouter(routers.SimpleRouter):
    routes = [
        routers.Route(
            url=r'^{prefix}/$',
            mapping={'get': 'list'},
            name='{basename}-list',
            detail=False,
            initkwargs={'suffix': 'list'}
        ),
        routers.Route(
            url=r'^{prefix}/get/{lookup}/$',
            mapping={'get': 'retrieve'},
            name='{basename}-detail',
            detail=True,
            initkwargs={'suffix': 'Detail'}
        )
    ]


class BasePaginator(pagination.PageNumberPagination):
    page_size = 10
    page_query_param = 'page'
    page_size_query_param = 'limit'


class PageViewSet(viewsets.ModelViewSet):
    lookup_field = 'pk'
    queryset = models.Page.objects.all()
    page_serializer = serializers.SimplePageSerializer
    serializer_class = serializers.PageSerializer
    pagination_class = BasePaginator
    renderer_classes = [JSONRenderer]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        request_url = urlparse(request.build_absolute_uri())
        domain = f'{request_url.scheme}://{request_url.netloc}'
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.page_serializer(page, many=True)
            urls = {instance['title']:f"{domain}{instance['url']}"
                    for instance in serializer.data}
            return self.get_paginated_response(urls)

        serializer = self.page_serializer(queryset, many=True)
        urls = {instance['title']:f"{domain}{instance['url']}"
                for instance in serializer.data}
        return Response(urls)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        serializer.order = request.query_params.get('order', 'asc')
        return Response(serializer.data)


@api_view(['GET'])
def healthcheck(request):
    return HttpResponse(status=200)