from django.urls import path

from just_media import views

router = views.ReadOnlyRouter()
router.register('page', views.PageViewSet)

urlpatterns = [
    path('healthcheck/', views.healthcheck),
]

urlpatterns += router.urls
