import os
from pathlib import Path

from django.db import migrations
from django.contrib.auth import get_user_model

current_app = Path(__file__).resolve().parent.parent.name


def generate_superuser(apps, schema_editor):
    """Create a new superuser """

    superuser = get_user_model().objects.create_superuser(
        username=os.environ.get('DJANGO_SUPERUSER_USERNAME', 'django_user'),
        email=os.environ.get('DJANGO_SUPERUSER_EMAIL', ''),
        password=os.environ.get('DJANGO_SUPERUSER_PASSWORD', 'django_pass'),
    )
    superuser.save()


class Migration(migrations.Migration):

    dependencies = [
        (current_app, "0001_initial"),
    ]

    operations = [
        migrations.RunPython(generate_superuser),
    ]
