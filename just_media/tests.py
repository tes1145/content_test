import time
import asyncio
from concurrent import futures

import requests
from django.test import TestCase


def get_page_sync():
    return requests.get('http://runserver:8000/api/page/get/1')


async def get_page_async():
    loop = asyncio.get_event_loop()
    future = loop.run_in_executor(None, get_page_sync)
    response = await future
    return response


async def get_page_map(task_count, max_workers=100, external_loop=None):
    loop = external_loop if external_loop else asyncio.new_event_loop()
    tasks = []
    while task_count > 0:
        for task_id in range(max_workers):
            if task_count <= 0 or len(tasks) >= max_workers:
                break
            tasks.append(loop.create_task(get_page_async()))
            task_count -= 1
        done, tasks = await asyncio.wait(
            tasks, return_when=futures.FIRST_COMPLETED)
        tasks = list(tasks)
        if task_count <= 0:
            break
    if len(tasks) != 0:
        done, tasks = await asyncio.wait(
            tasks, return_when=futures.ALL_COMPLETED)
    return True


class AnimalTestCase(TestCase):
    fixtures = ['just_media/fixtures/initial_data.json', ]

    def test_list_pages(self):
        response = requests.get('http://runserver:8000/api/page')
        self.assertEqual(response.status_code, 200)
        response = requests.get('http://runserver:8000/api/page',
                                params={'limit': 5})
        self.assertEqual(response.status_code, 200)
        response = requests.get('http://runserver:8000/api/page',
                                params={'limit': 1, 'page': 3})
        self.assertEqual(response.status_code, 200)

    def test_page_info(self):
        response = requests.get('http://runserver:8000/api/page/get/1')
        self.assertEqual(response.status_code, 200)
        response = requests.get('http://runserver:8000/api/page/get/1',
                                params={'order': 'desc'})
        self.assertEqual(response.status_code, 200)

    def test_counter(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        response = requests.get('http://runserver:8000/api/page/get/1')
        data = response.json()
        start_counter = data.get('content')[0].get('counter')
        n_requests = 400
        loop.run_until_complete(get_page_map(
            n_requests, max_workers=100, external_loop=loop
        ))
        time.sleep(10)
        response = requests.get('http://runserver:8000/api/page/get/1')
        data = response.json()
        end_counter = data.get('content')[0].get('counter')
        self.assertEqual(start_counter + n_requests + 1, end_counter)

